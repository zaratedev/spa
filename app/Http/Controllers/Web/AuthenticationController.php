<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\User;
use Socialite;

class AuthenticationController extends Controller
{
    public function redirectToProvider($provider)
    {
        try {
            return Socialite::driver($provider)->redirect();
        } catch (\InvalidArgumentException $e) {
            return redirect('/login');
        }
    }

    public function handleProviderCallback($provider)
    {
        $socialUser = Socialite::with($provider)->user();

        $user = User::where('provider_id', '=', $socialUser->id)
                ->where('provider', '=', $provider)
                ->first();

        if (!$user) {
            return redirect('/');
        }

        $user = User::create([
            'name'        => $socialUser->getName(),
            'email'       => $socialUser->getEmail(),
            'avatar'      => $socialUser->getAvatar(),
            'password'    => bcrypt(str_random(16)),
            'provider'    => $provider,
            'provider_id' => $socialUser->getId(),
        ]);

        Auth::login($user);
    }
}
