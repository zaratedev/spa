<?php

Route::prefix('v1')->namespace('Api')->name('api.')->group(function () {
    Route::get('cafes', 'CoffeeController@index')->name('coffee.index');
    Route::get('cafe/{id}', 'CoffeeController@show')->name('coffee.show');

    Route::middleware('auth:api')->group(function () {
        Route::post('cafes', 'CoffeeController@store')->name('coffee.store');
    });
});
