/*
 * ----------------------------------------------------------------------------
 * Vue Routes.
 * ---------------------------------------------------------------------------
 */

import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Vue.component('Home', require('./pages/Home.vue'))
        },
        {
            path: '/cafes',
            name: 'coffees',
            component: Vue.component('Coffee', require('./pages/Coffees.vue'))
        },
        {
            path: '/cafes/nuevo',
            name: 'newCoffee',
            component: Vue.component('NewCoffee', require('./pages/NewCoffee.vue'))
        },
        {
            path: '/cafes/:id',
            name: 'coffee',
            component: Vue.component('Home', require('./pages/Coffee.vue'))
        },
    ]
})
