/**
 *  Defines the API route we are using
 */
const api_url = '';

switch (process.env.NODE_ENV) {
    case 'development':
        api_url = 'https://spa.test/api/v1';
        break;
    case 'production':
        api_url = '';
        break;
    default:
}

export default SPA_CONFIG = {
    API_URL: api_url
}
