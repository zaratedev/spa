<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>SPA</title>

        <link rel="stylesheet" href="/css/app.css">

        <script type="text/javascript">
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
    </head>
    <body>
        <div class="app">
            <router-view></router-view>
        </div>

        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>
